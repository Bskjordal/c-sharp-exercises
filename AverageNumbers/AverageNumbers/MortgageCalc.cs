﻿using System;
namespace MortgageCalc;

class MortgageCalculation
{
    static void Main(string[] args)
    {
        double amtBorrowed;
        double interstRate;
        double monthlyInterestRate;
        int termYears;
        double payment;
        double termYearsMonth;
        double totPay;

        Console.Write("How much are you borrowing? ");
        amtBorrowed = Convert.ToDouble(Console.ReadLine());
        Console.Write("What is your interest rate? ");
        interstRate = Convert.ToDouble(Console.ReadLine());
        Console.Write("How long is your loan (in years)? ");
        termYears = Convert.ToInt32(Console.ReadLine());
        

        monthlyInterestRate = (interstRate / 1200);
        termYearsMonth = termYears * 12;
        
        payment=  (amtBorrowed * monthlyInterestRate) / (1 - Math.Pow((1 + monthlyInterestRate), -termYearsMonth));
        totPay = payment * termYearsMonth;

       Console.WriteLine($"Your estimated payment is: {payment}");
       Console.WriteLine($"You paid {totPay} over the life of the loan");





    }
}
