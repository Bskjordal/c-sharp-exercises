﻿namespace AvergingNumbers
{
    internal static class ProgramHelpers
    {
        static void Main(string[] args)
        {
            int num1 = 10;
            int num2 = 15;
            int num3 = 20;
            int num4 = 25;
            int divider = 4;
            double result = (num1 + num2 + num3 + num4) / (double)divider;

            Console.WriteLine($"{result}");


        }
    }
}