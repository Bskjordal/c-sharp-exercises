﻿namespace LearnCSharpApp
{
   internal class program
    {
        static void DisplayWelcome(string name)
        {
            Console.WriteLine ("Welcome " + name);
        }
        static void Main(string[] args)
        {
            Console.Write("Please enter your name: ");
            string name = Console.ReadLine();
            DisplayWelcome(name);
        
        }
    }
}
